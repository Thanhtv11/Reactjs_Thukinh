import React, { Component } from 'react'

export default class GlassItem extends Component {
    handleOnclick = (item) =>{
      this.props.HandleShowGlass(item)
    } 
  render() {
    let dataArr = this.props.Data
    return (
      <div style={{display:"flex", flexWrap:"wrap",width:"400px"}}>
        {dataArr.map(item=>{
            return (
                <div style={{margin:"20px"}} className='imageItem' key={item.id}>
                     <img 
                     onClick={()=> this.handleOnclick(item)} 
                     style={{width:80, cursor:'pointer',border:"2px solid white",borderRadius:"5px"}} 
                     src={item.url}></img>
                </div>
            ) 
        })}
      </div>
    )
  }
}
